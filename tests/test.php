<?php
declare(strict_types=1);

use JLanger\CSV\CSV;
use JLanger\CSV\CsvConfig;
use JLanger\CSV\CsvFile;
use JLanger\CSV\Exceptions\CsvException;

require_once __DIR__ . '/../vendor/autoload.php';

try {
$csvconfig = new CsvConfig();
$csvconfig->setHeadline(['headline13', 'headline2']);
$file = new CsvFile('testupload.csv');
$csv = new CSV($csvconfig);
    $fileArr = $csv->read($file);
} catch (CsvException $e) {
    trigger_error(get_class($e) . ': ' . $e->getMessage(), E_USER_ERROR);
}

echo '<pre>';
print_r($fileArr);
echo '</pre>';