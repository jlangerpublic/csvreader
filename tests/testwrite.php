<?php
declare(strict_types=1);

use JLanger\CSV\CSV;
use JLanger\CSV\CsvConfig;
use JLanger\CSV\Exceptions\CsvException;

require_once __DIR__ . '/../vendor/autoload.php';

$config = new CsvConfig();
$config->setSafePath('')
    ->setFilename('test.csv');

$input = [
        ['h1', 'h2'],
        [1, 2],
        ['l2', 'l3']
    ];

$csv = new CSV($config);
try {
    $link = $csv->write($input);
} catch (CsvException $e) {
    print_r($e->getMessage());
}

echo '<a href="' . $link['pathToFile'] . '">Link</a>';