<?php
declare(strict_types=1);

namespace JLanger\CSV;

use JLanger\CSV\Exceptions\FileNotFoundException;
use SplFileObject;
use function file_exists;

class CsvFile extends SplFileObject
{
    /** @var string */
    protected $fileName;

    /** @var string */
    public $delimiter;

    /** @var string */
    public $enclosure;

    /**
     * sets the SplFileObject.
     *
     * @param string $fileName
     * @param string $open_mode
     * @param bool   $use_include_path
     * @param null   $context
     */
    public function __construct(string $fileName, string $open_mode = 'r', bool $use_include_path = false, $context = null)
    {
        $this->fileName = $fileName;
        parent::__construct($fileName, $open_mode, $use_include_path, $context);
        
        
    }

    /**
     * @param string $delimiter *
     *
     * @return CsvFile
     */
    public function  setDelimiter(string $delimiter): CsvFile
    {
        $this->delimiter = $delimiter;

        return $this;
    }

    /**
     * @param string $enclosure *
     *
     * @return CsvFile
     */
    public function setEnclosure(string $enclosure): CsvFile
    {
        $this->enclosure = $enclosure;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName *
     *
     * @return CsvFile
     */
    public function setFileName(string $fileName): CsvFile
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Delete the file.
     */
    public function delete(): void
    {
        unlink($this->fileName);
    }
}