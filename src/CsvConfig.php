<?php
declare(strict_types=1);

namespace JLanger\CSV;


class CsvConfig
{
    /** @var int  */
    public const STORE_NEW = 0;
    
    /** @var int  */
    public const STORE_OVERWRITE = 1;
    
    /** @var int  */
    public const STORE_NO_OVERWRITE = 2;
    
    /** @var array */
    public $headline = [];
    
    /** @var string */
    public $delimiter = ',';
    
    /** @var string */
    public $enclosure = '"';
    
    /** @var string */
    public $escapeChar = '\\';
    
    /** @var string  */
    public $filename = '';
    
    /** @var string  - contains the path where csv-files should be stored.*/
    public $safePath = '';
    
    /** @var int  */
    public $storeBehavior = self::STORE_NEW;
    
    public function getConfig(): CsvConfig
    {
        return $this;
    }

    /**
     * @param array $headline *
     *
     * @return CsvConfig
     */
    public function setHeadline(array $headline): CsvConfig
    {
        $this->headline = $headline;

        return $this;
}

    /**
     * @param string $delimiter *
     *
     * @return CsvConfig
     */
    public function setDelimiter(string $delimiter): CsvConfig
    {
        $this->delimiter = $delimiter;

        return $this;
}

    /**
     * @param string $enclosure *
     *
     * @return CsvConfig
     */
    public function setEnclosure(string $enclosure): CsvConfig
    {
        $this->enclosure = $enclosure;

        return $this;
}

    /**
     * @param string $escapeChar *
     *
     * @return CsvConfig
     */
    public function setEscapeChar(string $escapeChar): CsvConfig
    {
        $this->escapeChar = $escapeChar;

        return $this;
}

    /**
     * @param string $safePath
     *
     * @return CsvConfig
     */
    public function setSafePath(string $safePath): CsvConfig
    {
        $this->safePath = $safePath;

        return $this;
}

    /**
     * @param int $storeBehavior
     *
     * @return CsvConfig
     */
    public function setStoreBehavior(int $storeBehavior): CsvConfig
    {
        $this->storeBehavior = $storeBehavior;

        return $this;
}

    /**
     * @param string $filename
     *
     * @return CsvConfig
     */
    public function setFilename(string $filename): CsvConfig
    {
        $this->filename = $filename;

        return $this;
}
}