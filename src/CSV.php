<?php
declare(strict_types=1);

namespace JLanger\CSV;

use JLanger\CSV\Exceptions\CsvException;
use JLanger\CSV\Exceptions\FormatException;
use JLanger\CSV\Exceptions\FileNotFoundException;
use JLanger\CSV\Exceptions\WrongHeadlineException;
use function array_push;
use function fclose;
use function file_exists;
use function fputcsv;
use function str_getcsv;
use function strcasecmp;
use function strpos;
use function strrpos;
use function substr;
use function unlink;
use function var_dump;

class CSV
{
    /** @var CsvFile */
    private $file;
    
    /** @var CsvConfig */
    private $config;

    /**
     * CSV constructor.
     *
     * @param CsvConfig $config
     */
    public function __construct(CsvConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Reads the csv-file and convert data to an array.
     * 
     * @param CsvFile $file
     *
     * @return array
     * @throws CsvException
     * @throws FormatException
     * @throws WrongHeadlineException
     */
    public function read(CsvFile $file): array
    {
        $this->file = $file;
        $config = $this->config;
        
        // Check if file is valid, throws Exceptions on error:
        if (file_exists($file->getFileName()) === false) {
            throw new FileNotFoundException();
        }
        $this->validateCsvFile($file);
        
        $fileArrTmp = [];
        while(!$file->eof()) {
            array_push($fileArrTmp, $file->fgets());
        }

        $fileArr = [];
        $count = 0;
        $headline = [];
        foreach ($fileArrTmp as $item) {
            if ($count === 0) {
                $headline = str_getcsv($item, $config->delimiter, $config->enclosure, $config->escapeChar);
                    if ($headline !== $config->headline) {
                        throw new WrongHeadlineException();
                    }
            } elseif ($count > 0) {
                $dataArrTmp = str_getcsv($item, $config->delimiter, $config->enclosure, $config->escapeChar);
                $dataArr = [];
                $x = 0;
                foreach($dataArrTmp as $data) {
                    $dataArr[$headline[$x]] = $data;
                    $x++;
                }
                array_push($fileArr, $dataArr);
            }
            
            $count++;
        }
        
        return $fileArr;
    }

    /**
     * Writes a csv-File to the filesystem. Returns the filename of the created file.
     * 
     * @param array $input
     *
     * @return array
     * @throws CsvException
     */
    public function write(array $input): array 
    {
        $filename = $this->config->filename;
        $pathToFile = $this->config->safePath . $filename;
        if ($this->config->storeBehavior === CsvConfig::STORE_NEW) {
            $pathToFile = $this->removeFileExtension($pathToFile);
            $pathToFileTmp = $pathToFile;
            $filename = $this->removeFileExtension($filename);
            $filenameTmp = $filename;
            $count = 1;
            while(file_exists($pathToFileTmp . '.csv')){
                $pathToFileTmp = $pathToFile . '_' . $count;
                $filenameTmp = $filename . '_' . $count;
                $count++;
            }
            $filename = $filenameTmp . '.csv';
            $pathToFile = $pathToFileTmp . '.csv';
        }
        if ($this->config->storeBehavior === CsvConfig::STORE_OVERWRITE) {
            if (file_exists($pathToFile)) {
                unlink($pathToFile);
            }
        }
        if ($this->config->storeBehavior === CsvConfig::STORE_NO_OVERWRITE) {
            if (file_exists($pathToFile)) {
                throw new CsvException('File already exists, mode is set to not overwrite files.');
            }
        }
        $file = fopen($pathToFile, 'x');
        foreach ($input as $data) {
            fputcsv($file, $data);
        }
        fclose($file);
        
        return ['pathToFile' => $pathToFile, 'filename' => $filename];
    }
    
    private function removeFileExtension(string $filename): string
    {
        return substr($filename, 0, strrpos($filename, "."));
    }
    
    private function iterateFilename(string $pathToFile, int $count): int 
    {
        return file_exists($pathToFile) ? $count + 1 : -1;
    }

    /**
     * Validates the csv-file.
     *
     * @param CsvFile $file
     *
     * @return bool
     * @throws FormatException
     */
    private function validateCsvFile(CsvFile $file): bool
    {
        // Check if delimiter, enclosure and escape character is valid:
        
        $control = $file->getCsvControl();
        $config = $this->config;
        if ($control[0] !== $config->delimiter) {
            throw new FormatException('wrong delimiter. Must be "' . $config->delimiter . '" but is "' . $control[0] . '"');
        }
        if ($control[1] !== $config->enclosure) {
            throw new FormatException('wrong enclosure. Must be "' . $config->enclosure . '" but is "' . $control[1] . '"');
        }
        if ($control[2] !== $config->escapeChar) {
            throw new FormatException('wrong escape character. Must be "' . $config->escapeChar . '" but is "' . $control[2] . '"');
        }
        
        return true;
    }
}