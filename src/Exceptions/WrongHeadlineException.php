<?php
declare(strict_types=1);

namespace JLanger\CSV\Exceptions;


use Throwable;

class WrongHeadlineException extends CsvException
{
    public function __construct($message = 'wrong headline.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}