<?php
declare(strict_types=1);

namespace JLanger\CSV\Exceptions;

use Throwable;

class FileNotFoundException extends CsvException
{
    public function __construct($message = 'Could not find CSV-File.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}