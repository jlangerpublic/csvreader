<?php
declare(strict_types=1);

namespace JLanger\CSV\Exceptions;


use Throwable;

class FormatException extends CsvException
{
    public function __construct($message = 'Incorrect File Format.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}