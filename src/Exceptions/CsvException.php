<?php
declare(strict_types=1);

namespace JLanger\CSV\Exceptions;


use Exception;
use Throwable;

class CsvException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}