# csvreader

Minimal Usage:

```php
use JLanger\CSV\CSV;
use JLanger\CSV\CsvConfig;
use JLanger\CSV\CsvFile;
use JLanger\CSV\Exceptions\CsvException;

require_once 'path-to-vendor-folder/vendor/autoload.php';

try {
    $config = new CsvConfig();
    
    // setzen der Headline:
    $headlineArray = ['value1', 'value2', '...'];
    $config->setHeadline($headlineArray);

    $file = new CsvFile('path to file.csv');
    $csv = new CSV($csvconfig);
    $fileArr = $csv->read($file);
} catch (CsvException $e) {
    trigger_error(get_class($e) . ': ' . $e->getMessage(), E_USER_NOTICE);
}
```
Options:\
`$config->setDelimiter('delimiter')` sets the delimiter, default: `,`  
`$config->setEnclosure('enclosure')` sets the enclosure, default: `"`  
`$config->setEscapeChar('char')` sets the escape character, default: `\`  

Example for writing a csv-file:
```php
<?php
declare(strict_types=1);

use JLanger\CSV\CSV;
use JLanger\CSV\CsvConfig;
use JLanger\CSV\Exceptions\CsvException;

require_once __DIR__ . '/../vendor/autoload.php';

$config = new CsvConfig();
$config->setSafePath('')
    ->setFilename('test.csv');

$input = [
        ['h1', 'h2'],
        [1, 2],
        ['l2', 'l3']
    ];

$csv = new CSV($config);
try {
    $link = $csv->write($input);
} catch (CsvException $e) {
    print_r($e->getMessage());
}

echo '<a href="' . $link['pathToFile'] . '">Link</a>';
```

All errors will throw an CsvException.